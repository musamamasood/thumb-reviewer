<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.datumsquare.com/
 * @since      1.0.0
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/admin
 * @author     Masood <masood.u@allshoreresources.com>
 */
class Thumb_Reviewer_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->register_review_posttype();

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Thumb_Reviewer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Thumb_Reviewer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/thumb-reviewer-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Thumb_Reviewer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Thumb_Reviewer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/thumb-reviewer-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register private post type for review
	 *
	 * @since 1.0
	 * @return null
	 */
	private function register_review_posttype(){
		$review_posttype = new CPT(array(
			'post_type_name' => 'review',
			'singular'		 => 'Review',
			'plural'		 => 'Reviews',
			'slug'			 => 'review',
		), array(
			'capability_type'=> 'page',
			'hierarchical'	 => true,
			'has_archive'	 => true,
			'menu_icon'		 => 'dashicons-thumbs-up',
			'menu_position'	 => 20,
			'supports'		 => array('title', 'editor', 'custom-fields', 'page-attributes')
		));
		$review_posttype->columns(array(
			'cb'			=>	'<input type="checkbox" />',
			'title' 		=>  __( 'Title' ),
			'author' 		=>  __( 'Author' ),
			'reviewer_post' =>  __( 'Reviewed Post' ),
			'reviewer_name' =>  __( 'Reviewer Name' ),
			'date' 			=>  __( 'Date' ),
		));
		$review_posttype->populate_column('reviewer_post', function($column, $post){

			$review_post_id = get_post_meta( $post->ID, 'reviewer_post', true );
			echo '<a href="'. get_edit_post_link( $review_post_id ) .'" target="_BLANCK">'
					.get_the_title( $review_post_id ).
				  '</a>';
		});
		$review_posttype->populate_column('reviewer_name', function($column, $post){
			echo get_post_meta( $post->ID, 'reviewer_name', true );
		});
		$review_posttype->sortable(array(
		    'reviewer_post' => array('reviewer_post', false),
		    'reviewer_name' => array('reviewer_name', false)
		));
	}

	/**
	 * Defining the filter that will be used so we can select posts by 'Reviewed Post'
	 *
	 * @since 1.0
	 * @return string
	 */
	public function add_post_filter_to_reviews_administration(){

		if (isset($_GET['post_type'])) {
	        $post_type = $_GET['post_type'];
	        if (post_type_exists($post_type) && $post_type=='review') {
	            global $wpdb;
	            $sql= "SELECT pm.meta_value FROM {$wpdb->postmeta} pm
					INNER JOIN {$wpdb->posts} p ON p.ID=pm.post_id
					WHERE p.post_type='review' AND pm.meta_key='reviewer_post'
					GROUP BY pm.meta_value
					ORDER BY pm.meta_value";
	            $results = $wpdb->get_results($sql);
	            $html = array();
	            $html[] = "<select id=\"sortby\" name=\"sortby\">";
	            $html[] = "<option value=\"None\">All Reviewed Posts</option>";
	            $this_sort = ( isset($_GET['sortby']) ) ? $_GET['sortby'] : null;
	            foreach($results as $meta_key) {
	                $default = ($this_sort==$meta_key->meta_value ? ' selected="selected"' : '');
	                $value = esc_attr($meta_key->meta_value);
	                $value = get_the_title( $value );
	                $html[] = "<option value=\"{$meta_key->meta_value}\"$default>{$value}</option>";
	            }
            $html[] = "</select>";
            echo implode("\n",$html);
	        }
	    }
	}

	/**
	 * Defining the filter that will be used so we can parse query to sort posts by 'Reviewed Post'.
	 *
	 * @since 1.0
	 * @return string
	 */
	public function sort_review_by_meta_value($query) {
	    global $pagenow;
	    if (is_admin() && $pagenow=='edit.php' &&
	        isset($_GET['post_type']) && $_GET['post_type']=='review' &&
	        isset($_GET['sortby'])  && $_GET['sortby'] !='None')  {
	        $query->query_vars['orderby'] = 'meta_value';
	        $query->query_vars['meta_key'] = 'reviewer_post';
	        $query->query_vars['meta_value'] = $_GET['sortby'];
	    }
	}
}
