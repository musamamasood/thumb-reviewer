<?php

/**
 * The filter-specific functionality of the plugin.
 *
 * @link       http://www.datumsquare.com/
 * @since      1.0.0
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/admin
 */

/**
 * The filter-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/admin
 * @author     Masood <masood.u@allshoreresources.com>
 */
class Thumb_Reviewer_Filter {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register filter to exclude the post type for thumbs.
	 *
	 * @since    1.0.0
	 */
	public function wp_thumb_exclude_post_type( $excluded ){
		$excluded[] = 'page';
		return $excluded;
	}

}
