<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.datumsquare.com/
 * @since      1.0.0
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/includes
 * @author     Masood <masood.u@allshoreresources.com>
 */
class Thumb_Reviewer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		add_option( 'tr_db_version', '1.0' );
		$charset_collate = $wpdb->get_charset_collate();
		$table_name = $wpdb->prefix . TR_THUMB_REVIEW;
		if ( function_exists( 'is_multisite' ) && is_multisite() ) {
			$table_name = $wpdb->base_prefix . 'thumb_review';
		}

		$review_table = "CREATE TABLE IF NOT EXISTS $table_name (
	      id int(11) NOT NULL auto_increment,
	      blog_id int(11) NOT NULL,
	      post_id int(11) NOT NULL,
		  user_id int(11) NOT NULL,
		  user_ip varchar(55) NOT NULL,
		  thumb tinyint(1) DEFAULT 0 NOT NULL,
	      time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	      UNIQUE KEY id (id),
	      PRIMARY KEY id (id)
	    ) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
		dbdelta( $review_table );
	}

}
