<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.datumsquare.com/
 * @since      1.0.0
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/includes
 * @author     Masood <masood.u@allshoreresources.com>
 */
class Thumb_Reviewer_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
