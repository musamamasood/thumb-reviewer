<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.datumsquare.com/
 * @since      1.0.0
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/includes
 * @author     Masood <masood.u@allshoreresources.com>
 */
class Thumb_Reviewer {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Thumb_Reviewer_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'thumb-reviewer';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

		if ( !defined('TR_THUMB_REVIEW')) {
			define('TR_THUMB_REVIEW', 'thumb_review');
		}
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Thumb_Reviewer_Loader. Orchestrates the hooks of the plugin.
	 * - Thumb_Reviewer_i18n. Defines internationalization functionality.
	 * - Thumb_Reviewer_Admin. Defines all hooks for the admin area.
	 * - Thumb_Reviewer_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-thumb-reviewer-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-thumb-reviewer-i18n.php';

		/**
		 *  The class responsible for adding filter functionality that hookable.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-thumb-reviewer-filter.php';

		/**
		 * The class responsible for defining captcha functionality for review form.
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-thumb-reviewer-captcha.php';

		/**
		 * The class responsible for defining post_type functionality for review.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-thumb-reviewer-cpt.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-thumb-reviewer-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-thumb-reviewer-public.php';

		$this->loader = new Thumb_Reviewer_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Thumb_Reviewer_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Thumb_Reviewer_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Thumb_Reviewer_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'restrict_manage_posts', $plugin_admin, 'add_post_filter_to_reviews_administration' );
		$this->loader->add_filter( 'parse_query', $plugin_admin, 'sort_review_by_meta_value' );

		//session support
		$this->loader->add_action( 'init',  $this, 'tr_session_start', 10, 1);
		$this->loader->add_action( 'wp_logout', $this , 'tr_session_destory');
		$this->loader->add_action( 'wp_login', $this, 'tr_session_start' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Thumb_Reviewer_Public( $this->get_plugin_name(), $this->get_version() );
		$plugin_filter = new Thumb_Reviewer_Filter( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'the_content', $plugin_public, 'wp_thumb_review_inject_data' );
		// ajax action
		$this->loader->add_action( 'wp_ajax_set_post_thumb', $plugin_public, 'set_post_thumb');
		$this->loader->add_action( 'wp_ajax_nopriv_set_post_thumb', $plugin_public, 'set_post_thumb');
		$this->loader->add_action( 'wp_ajax_set_post_review', $plugin_public, 'set_post_review');
		$this->loader->add_action( 'wp_ajax_nopriv_set_post_review', $plugin_public, 'set_post_review');

		// add filter functionality
		$this->loader->add_filter( 'wp_thumb_excluded_post_types', $plugin_filter, 'wp_thumb_exclude_post_type' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Thumb_Reviewer_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Session register on initlization
	 *
	 * @since 1.0.0
	 * @return null
	 */
	public function tr_session_start(){
		if ( !session_id() ) {
			session_start();
		}
	}

	/**
	 * Session destory on logout.
	 *
	 * @since 1.0.0
	 * @return null
	 */
	public function tr_session_destory(){
		session_destroy();
	}

}
