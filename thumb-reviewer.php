<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.datumsquare.com/
 * @since             1.0.0
 * @package           Thumb_Reviewer
 *
 * @wordpress-plugin
 * Plugin Name:       Thumb Reviewer
 * Plugin URI:        http://www.datumsquare.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Masood
 * Author URI:        http://www.datumsquare.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       thumb-reviewer
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-thumb-reviewer-activator.php
 */
function activate_thumb_reviewer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-thumb-reviewer-activator.php';
	Thumb_Reviewer_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-thumb-reviewer-deactivator.php
 */
function deactivate_thumb_reviewer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-thumb-reviewer-deactivator.php';
	Thumb_Reviewer_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_thumb_reviewer' );
register_deactivation_hook( __FILE__, 'deactivate_thumb_reviewer' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-thumb-reviewer.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_thumb_reviewer() {

	$plugin = new Thumb_Reviewer();
	$plugin->run();

}
run_thumb_reviewer();
