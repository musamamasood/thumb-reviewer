<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.datumsquare.com/
 * @since      1.0.0
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Thumb_Reviewer
 * @subpackage Thumb_Reviewer/public
 * @author     Masood <masood.u@allshoreresources.com>
 */
class Thumb_Reviewer_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version, $captcha;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->captcha = new Thumb_Reviewer_Captcha;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Thumb_Reviewer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Thumb_Reviewer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/thumb-reviewer-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Thumb_Reviewer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Thumb_Reviewer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		global $post, $blog_id, $user_ID;
		$tr_var = array(
			'blog_id'	=> $blog_id,
			'post_id' 	=> ( $post ) ? $post->ID: 0,
			'user_id'	=> $user_ID,
			'nonce' 	=> wp_create_nonce( 'tr_nonce' ),
			'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
			);
		wp_enqueue_script( 'jquery_validate', plugin_dir_url( __FILE__ ) . 'js/jquery.validate.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/thumb-reviewer-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'tr', $tr_var );
	}


	/**
	 * Get the meta box data.
	 *
	 * @param  [string] $content [post content]
	 * @return [string] $content [after adding review in post content]
	 * @since 1.0
	 */
	public function wp_thumb_review_get_data( $post_id = null ) {
		global $post, $blog_id, $user_ID, $wpdb;
		$table_name = $wpdb->prefix . TR_THUMB_REVIEW;
		#$options = get_option('wp_review_options');

		if (empty($post_id))
			$post_id = $post->ID;

		/* Retrieve the meta box data. */
		/*$heading     = get_post_meta( $post_id, 'wp_review_heading', true );
		$desc_title  = get_post_meta( $post_id, 'wp_review_desc_title', true );
		if ( ! $desc_title ) $desc_title = __('Summary', 'wp-review');
		$desc        = get_post_meta( $post_id, 'wp_review_desc', true );
		$items       = get_post_meta( $post_id, 'wp_review_item', true );
		$type        = get_post_meta( $post_id, 'wp_review_type', true );
		$total       = get_post_meta( $post_id, 'wp_review_total', true );
		$hide_desc   = get_post_meta( $post_id, 'wp_review_hide_desc', true );
		$allowUsers  = get_post_meta( $post_id, 'wp_review_userReview', true );

		$colors = array();
		$colors['custom_colors'] = get_post_meta( $post_id, 'wp_review_custom_colors', true );
		$colors['color'] = get_post_meta( $post_id, 'wp_review_color', true );
		$colors['type']  = get_post_meta( $post_id, 'wp_review_type', true );
		$colors['fontcolor'] = get_post_meta( $post_id, 'wp_review_fontcolor', true );
		$colors['bgcolor1']  = get_post_meta( $post_id, 'wp_review_bgcolor1', true );
		$colors['bgcolor2']  = get_post_meta( $post_id, 'wp_review_bgcolor2', true );
		$colors['bordercolor']  = get_post_meta( $post_id, 'wp_review_bordercolor', true );
		$colors['total'] = get_post_meta( $post_id, 'wp_review_total', true );

	    if (!$colors['custom_colors'] && !empty($options['colors']) && is_array($options['colors'])) {
			$colors = array_merge($colors, $options['colors']);
		}
	    $colors = apply_filters('wp_review_colors', $colors, $post_id);
	    $color = $colors['color'];*/

	    /* Define a custom class for bar type. */
	    $class    = '';
	    // if ( 'point' == $type ) {
	    // 	$class = 'bar-point';
	    // } elseif ( 'percentage' == $type ) {
	    // 	$class = 'percentage-point';
	    // }
	    $post_types = get_post_types( array('public' => true), 'names' );
	    $excluded_post_types = apply_filters('wp_thumb_excluded_post_types', array('attachment'));
	    $allowed_post_types = array();
	    foreach ($post_types as $i => $post_type) {
	    	if (!in_array($post_type, $excluded_post_types)) {
	            $allowed_post_types[] = $post_type; // allow it if it's not excluded
	        }
	    }

		/**
		 * Add the custom data from the meta box to the main query an
		 * make sure the hook only apply on allowed post types
		 */
		if ( is_singular($allowed_post_types) && is_main_query() ) {
			//if ( $type != '' && is_main_query() && in_array(get_post_type($post_id), $allowed_post_types)) {
			// using this second if() instead of the first will allow reviews to be displayed on archive pages, but it may mess up excerpts
			$review = '<div itemscope itemtype="http://schema.org/Review" id="review" class="review-wrapper wp-review-'.$post_id.' ' . $class . ' delay-animation" >';
			$review .= '<h3>Rate Post: </h3>';

			## add like and dislike
			$likes = $wpdb->get_var( $wpdb->prepare("SELECT count(id) as t FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND thumb = '1' group by thumb", $blog_id, $post_id) );
			$dislike = $wpdb->get_var( $wpdb->prepare("SELECT count(id) as t FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND thumb = '0' group by thumb", $blog_id, $post_id) );
			$like 	 = ( isset($likes) ) ? $likes : 0;
			$dislike = ( isset($dislike) ) ? $dislike : 0;
			$review .= '<div class="tr-ur-like-wrap tr-on">';
			$review .= '<div class="tr-dislike">
							<span data-method="dislike" class="dislike" data-rating-id="rwp_rating_5658a43bd2815" data-post-id="1" data-user-id="0"></span>
							<em>' .$dislike. '</em>
						</div>';
			$review .= '<div class="tr-like">
							<em>' .$like. '</em>
							<span data-method="like" data-rating-id="rwp_rating_5658a43bd2815" data-post-id="1" data-user-id="0"></span>
						</div>';
			$review .=  '<div class="rwp-loader" style="display: none;"></div>';
			$review .= '</div><div class="clear"></div>';

			//$this->captcha->simple_php_captcha();
			$review .= $this->review_form_html();

			// review array
			$reviews_data = get_posts('post_type=review&meta_key=reviewer_post&meta_value='.$post_id);

			if ( $reviews_data ) {
				$review .='<div class="review-title"><h2>Thumb Reviews</h2></div><div class="thumbs-reviews">';
				$review_arr = array();
				$counter = 0;
				foreach($reviews_data as $r):
					$review_arr[$counter]['name']   = get_post_meta( $r->ID, 'reviewer_name', true );
					$reviewer_thumb_id 	  			= get_post_meta( $r->ID, 'reviewer_thumb_id', true );
					$reviewer_thumb_id	  			= ( $reviewer_thumb_id ) ? $reviewer_thumb_id : 1;

					$review_arr[$counter]['status'] = $wpdb->get_var( $wpdb->prepare("SELECT thumb as t FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND id = '%d'", $blog_id, $post_id, $reviewer_thumb_id) );

					$review_arr[$counter]['content']= $r->post_content;
					$review_arr[$counter++]['date'] = $r->post_date;
				endforeach;

				$review .='<div class="thumbsup">';
				foreach ($review_arr as $tr):
					if ($tr['status'] == 1) {
						$review .= '<div class="thumbs-title">Reviews Thumbs Up</div>
									<div class="review-meta">
									<span>Review by:<strong>'.$tr['name'].'</strong> on '. $tr['date'].'</span>
									</div>
									<div class="review-content">
										<p>'.$tr['content'].'</p>
									</div>';
					}
				endforeach;

				$review .='</div>';
				$review .= '<div class="thumbsdown">';

				foreach ($review_arr as $tr):
					if ($tr['status'] == 0) {
							$review .= '<div class="thumbs-title">Reviews Thumbs Down</div>
							<div class="review-meta">
								<span>Review by: <strong>'.$tr['name'].'</strong> on '.$tr['date'].'</span>
							</div>
							<div class="review-content">
								<p>'.$tr['content'].'</p>
							</div>';
					}
				endforeach;

				$review .='</div>';
				$review .= '</div>';
			} // end review_data
			$review .= '</div><!-- #review --><div class="clear"></div>';

            return $review;
        } else {
        	return '';
        }
	}

	/**
	 * [wp_thumb_review_inject_data description]
	 *
	 * @param  [string] $content [post content]
	 * @return [string] $content [after adding review in post content]
	 * @since  1.0
	 */
	public function wp_thumb_review_inject_data( $content ) {
		global $post;
	    # TODO: we will include location option at option page to show thumb location.
	    /*$options = get_option('wp_review_options');
		$custom_location = get_post_meta( $post->ID, 'wp_review_custom_location', true );
		$location = get_post_meta( $post->ID, 'wp_review_location', true );
		if (!$custom_location && !empty($options['review_location'])) {
			$location = $options['review_location'];
		}

		$location = apply_filters('wp_review_location', $location, $post->ID);

	    if (empty($location) || $location == 'custom' || ! is_main_query() || ! in_the_loop()) {
	        return $content;
	    }*/
	    if ( ! is_main_query() || ! in_the_loop() ) {
	    	return $content;
	    }
	    $review = $this->wp_thumb_review_get_data();

	    global $multipage, $numpages, $page;
	    if( $multipage ) {
	    	if ($page == $numpages) {
	    		return $content .= $review;
	    	} else {
	    		return $content;
	    	}
	    } else {
	    	return $content .= $review;
	    }
        # TODO: we will include the location and use this section.
	    /*if ( 'bottom' == $location ) {
	        global $multipage, $numpages, $page;
	        if( $multipage ) {
	            if ($page == $numpages) {
	                return $content .= $review;
	            } else {
	                return $content;
	            }
	        } else {
	            return $content .= $review;
	        }
		} elseif ( 'top' == $location ) {
			return $review .= $content;
		} else {
	        return $content;
	    }*/
	}

	/**
	 * Check if user has reviewed this post previously
	 *
	 * @param  [type]  $post_id [description]
	 * @param  [type]  $user_id [description]
	 * @param  [type]  $ip      [description]
	 * @return boolean          [description]
	 */
	public function has_previous_review( $post_id, $user_id, $ip, $type = 'thumb' ){
		global $wpdb;
		global $blog_id;
		$table_name = $wpdb->prefix . TR_THUMB_REVIEW;

		if ( $type == 'review') {
			if( is_numeric( $user_id ) && $user_id > 0 ){

				$args = array (
					'post_type'              => 'review',
					'author'                 => $user_id,
					'meta_query'             => array(
						array(
							'key'       => 'reviewer_post',
							'value'     => $post_id,
							'compare'   => '=',
						),
					),
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) return true;
			} elseif( $ip != '' ){
				$thumb_id = $wpdb->get_row( $wpdb->prepare("SELECT id FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND user_ip = '%s' AND user_id = '0'", $blog_id, $post_id, $ip) );
				$args = array (
					'post_type'              => 'review',
					'meta_query'             => array(
						array(
							'key'       => 'reviewer_thumb_id',
							'value'     => $thumb_id->id,
							'compare'   => '=',
						)
					),
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) return true;
			}
			//return false; #TODO: need to validate reviews.

		}else{
			// Type is thumb
			if( is_numeric( $post_id ) && $post_id > 0 ){

				if (function_exists('is_multisite') && is_multisite()) {$table_name = $wpdb->base_prefix . TR_THUMB_REVIEW;}
				if( is_numeric( $user_id ) && $user_id > 0 ){
					$prevRates = $wpdb->get_row( $wpdb->prepare("SELECT COUNT(id) as reviewsNum FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND user_id = '%d'", $blog_id, $post_id, $user_id) );
					if( $prevRates->reviewsNum > 0 ) return true; else return false;
				} elseif( $ip != '' ){
					$prevRates = $wpdb->get_row( $wpdb->prepare("SELECT COUNT(id) as reviewsNum FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND user_ip = '%s' AND user_id = '0'", $blog_id, $post_id, $ip) );
					if( $prevRates->reviewsNum > 0 ) return true; else return false;
				}
				else return false;
			}
		}
		return false;
	}

	/**
	 * [get_post_thumbs description]
	 * @param  [type] $post_id [description]
	 * @return [type]          [description]
	 */
	public function get_post_thumbs( $post_id ){
		if( is_numeric( $post_id ) && $post_id > 0 ){
			global $wpdb;
			global $blog_id;
			$table_name = $wpdb->prefix . TR_THUMB_REVIEW;
			if (function_exists('is_multisite') && is_multisite()) {$table_name = $wpdb->base_prefix . TR_THUMB_REVIEW;
			} ##TODO: fix the query
			$reviews = $wpdb->get_results( $wpdb->prepare("SELECT ROUND( AVG(thumb) ,1 ) as reviewsAvg, COUNT(id) as reviewsNum FROM $table_name WHERE blog_id = '%d' AND post_id = '%d'", $blog_id, $post_id) );
			return $reviews;
		}
	}

	/**
	 * Set thumbup with Ajax
	 */
	public function set_post_thumb(){
	    // security #TODO
		check_ajax_referer( 'tr_nonce', 'nonce' );

		global $wpdb;
		$fields = array(
			'blog_id',
			'post_id',
			'user_id',
			'thumb',
		);
		$res = array( 'code' => 400, 'data'=> array( 'msg' => 'The form was not submitted correctly!' ) );

		// Check if fields are set
		foreach ($fields as $key) {
			if( !isset( $_POST[ $key ] ) )
				die( json_encode( $res ) );
		}

		$table_name = $wpdb->prefix . TR_THUMB_REVIEW;
		if (function_exists('is_multisite') && is_multisite()) {
			$table_name = $wpdb->base_prefix . TR_THUMB_REVIEW;
		}

		global $blog_id;
		$post_id = intval($_POST['post_id']);
		$user_id = intval($_POST['user_id']);

	    //$ip = $_SERVER['REMOTE_ADDR'];
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$uip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$uip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$uip = $_SERVER['REMOTE_ADDR'];
		}

		if (!$this->has_previous_review($post_id, $user_id, $uip)) {
			// Incrementing count
			$method = $_POST['thumb'];
			$thumb = 0;
			if( $method == 'like' ) {
				$thumb = 1;
			}

			$rows_affected = $wpdb->insert( $table_name, array('blog_id' => $blog_id, 'post_id' => $post_id, 'user_id' => $user_id, 'user_ip' => $uip, 'thumb' => $thumb) );
			$_SESSION['thumb_id'] = $wpdb->insert_id;

			if( $rows_affected ){
				//$reviews = $wpdb->get_results( $wpdb->prepare("SELECT count(id) as t FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' group by thumb order by thumb", $blog_id, $post_id) );
				$likes = $wpdb->get_var( $wpdb->prepare("SELECT count(id) as t FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND thumb = '1' group by thumb", $blog_id, $post_id) );
				$dislike = $wpdb->get_var( $wpdb->prepare("SELECT count(id) as t FROM $table_name WHERE blog_id = '%d' AND post_id = '%d' AND thumb = '0' group by thumb", $blog_id, $post_id) );
				$like 	 = ( isset($likes) ) ? $likes : 0;
				$dislike = ( isset($dislike) ) ? $dislike : 0;
	    		// TODO: customize to show the thumb and thumb down data later
				// Success!
				//
				$res['code'] = 200;
				$res['data'] = array(
					'msg' => __( 'Done', 'reviewer' ),
					'like' => $like,
					'dislike' => $dislike
				);
				exit(json_encode($res));
			} else {
				die( json_encode( $res ) );
			}
		} else {
			//code for this will be 400
			$res = array( 'code' => 200, 'data'=> array( 'msg' => 'You have already rated!' ) );
			die( json_encode( $res ) );
		}
		wp_die();
	}

	/**
	 * Set review with Ajax
	 */
	public function set_post_review(){
	    // security #TODO
		check_ajax_referer( 'tr_nonce', 'nonce' );

		global $wpdb;
		$fields = array(
			'blog_id',
			'post_id',
			'user_id',
			'tr_name',
			'tr_email',
			'tr_title',
			'tr_comment',
		);
		$res = array( 'code' => 400, 'data'=> array( 'msg' => 'The form was not submitted correctly!' ) );

		// Check if fields are set
		foreach ($fields as $key) {
			if( !isset( $_POST[ $key ] ) )
				die( json_encode( $res ) );
		}

		foreach ($fields as $key) {
			if( $_POST[ $key ] == '' )
				die( json_encode( $res ) );
		}

		if ( !is_email( $_POST['tr_email'] ) ) {
			$res = array( 'code' => 400, 'data'=> array( 'msg' => 'Please input valid email!' ) );
			die( json_encode( $res ) );
		}

		global $blog_id;
		$post_id = intval($_POST['post_id']);
		$user_id = intval($_POST['user_id']);
		if ( $_SESSION['thumb_id'] )
			$thumb_id = intval($_SESSION['thumb_id']);

	    //$ip = $_SERVER['REMOTE_ADDR'];
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$uip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$uip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$uip = $_SERVER['REMOTE_ADDR'];
		}

		// allowed html tags
		$allowed_tags = array(
		    'br' => array(),
		    'em' => array(),
		    'strong' => array(),
		);

		if (!$this->has_previous_review($post_id, $user_id, $uip, 'review')) {
			// Incrementing count
			$review   = array();
			$review['post_title'] 	= ( $_POST['tr_title'] ) ? sanitize_text_field( $_POST['tr_title'] ) : null;
			$review['post_content'] = ( $_POST['tr_comment'] ) ? wp_kses( $_POST['tr_comment'],  $allowed_tags) : null;
			$review['post_status'] 	= 'draft';
			$review['post_type'] 	= 'review';
			$review['post_author'] 	= ( $user_id ) ? $user_id : 0;
			$review_id = wp_insert_post( $review );

			if( $review_id ){
				$reviewer_name 	= ( $_POST['tr_name'] ) ? sanitize_text_field( $_POST['tr_name']  ) : null;
				$reviewer_email = ( $_POST['tr_email'] ) ? $_POST['tr_email'] : null;
				add_post_meta( $review_id, 'reviewer_post', $post_id );
				add_post_meta( $review_id, 'reviewer_name', $reviewer_name );
				add_post_meta( $review_id, 'reviewer_email', $reviewer_email );
				add_post_meta( $review_id, 'reviewer_thumb_id', $thumb_id );
				##add thumb table id to this review post.

				$current_review =  get_post( $review_id );
	    		$res['code'] = 200;
				$res['data'] = array(
					'msg' => __( 'Done', 'reviewer' ),
					'title'  => $current_review->post_title,
					'review' => $current_review->post_content
				);
				exit( json_encode($res) );
			} else {
				die( json_encode( $res ) );
			}
		} else {
			//code for this will be 400
			$res = array( 'code' => 200, 'data'=> array( 'msg' => 'You have already rated!' ) );
			die( json_encode( $res ) );
		}
		wp_die();
	}

	/**
	 * Review form html
	 *
	 * @since 1.0.0
	 */
	public function review_form_html(){

		$review_form  = '<form method="post" id="tr-review-form">';
		$review_form .= '<input type="text" name="tr_name" placeholder="Write your name">';
		$review_form .= '<input type="text" name="tr_email" placeholder="Write your email">';
		$review_form .= '<input type="text" name="tr_title" placeholder="Write your review title">';
		//$review_form .= wp_editor( '', 'mycustomeditor' );
		$review_form .= '<textarea name="tr_comment" placeholder="Write your review"></textarea>';
		$review_form .= '<p class="tr-submit-wrap">
							<input class="tr-submit-ur" type="button" value="Submit" />
							<input class="tr-cancel-ur" type="button" value="Cancel" />
							<span class="tr-loader"></span>
							<span class="tr-notification"></span>
						</p>';
		$review_form .= '</form>';

		return $review_form;
	}
}
