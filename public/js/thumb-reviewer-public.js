(function( $ ) {
	'use strict';

	 /*jshint smarttabs:true, regexp:false, browser:true */
	 /*globals tr, jQuery */
	 /*jshint -W030 */

	$( window ).load(function() {
		//Thumb Submitted
		$('.tr-on').on('click', '.tr-like span, .tr-dislike span', function(){
			var thumb = $(this).data('method'),
				n = $(this).parent().parent(),
				d = n.find('.rwp-loader'),
				tr_data = {
					action: 'set_post_thumb',
					blog_id: tr.blog_id,
					post_id: tr.post_id,
					user_id: tr.user_id,
					nonce: tr.nonce,
					thumb: thumb
				};
			d.fadeIn('fast');
			var p = $(this).parent().find('em');

			$('#tr-review-form').fadeIn();

			$.post(tr.ajaxurl, tr_data, function(a) {
	            switch (d.fadeOut('fast'), a.code) {
	                case 400:
	                    alert(a.data.msg);
	                    break;
	                case 200:
	                    var e = 'like' == thumb ? a.data.like : a.data.dislike;
	                    p.html(e), n.removeClass('rwp-on').addClass('rwp-off');
	                    break;
	                default:
	                    alert('Error in like/dislike process!');
	            }
	        }, 'json').fail(function() {
	            d.fadeOut('fast'), alert('Error during like/dislike process!');
	        });
		});
		// Review Submitted
		$('.tr-submit-ur').on('click', function(){
			var form = $(this).parent().parent(),
				d 	 = form.find('.rwp-loader'),
				tr_data = form.serialize() + '&action=set_post_review&blog_id='+tr.blog_id+'&post_id='+tr.post_id+'&user_id='+tr.user_id+'&nonce='+tr.nonce;
			d.fadeIn('fast');
			form.validate({
		        onfocusout: function (element) {
		            this.element(element);
		        },
		        rules: {
		            tr_name: {
		                required: true,
		                minlength: 5
		            },
		            tr_email: {
		                required: true,
		                email: true
		            },
		            tr_title: {
		                required: true,
		                minlength: 5
		            },
		            tr_comment: {
		                required: true,
		                minlength: 100,
		                maxlength: 800
		            }
		        },
		        messages: {
		            tr_name: 'Please enter your name.',
		            tr_email: 'Please enter your email.',
		            tr_title:  'Please enter your review title.',
		            tr_comment:'Please enter your review message.',
		        },
		        errorElement: 'span',
		        errorPlacement: function (error, element) {
		            element.after(error);
		        }
		    });
			if (!form.valid()) return;
			$.post(tr.ajaxurl, tr_data, function(a) {
	            switch (d.fadeOut('fast'), a.code) {
	                case 400:
	                    alert(a.data.msg);
	                    break;
	                case 200:
	                    alert( a.data.msg );
	                    form.fadeOut('slow');
	                    break;
	                default:
	                    alert('Error in submit review process!');
	            }
	        }, 'json').fail(function() {
	            d.fadeOut('fast'), alert('Error during submit review process!');
	        });
		});

		$('.tr-cancel-ur').click(function() {
			$('#tr-review-form').fadeOut();
		});

	});
})( jQuery );
